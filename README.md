# Cistercian Counter

This is a counter program with a GUI that displays the appropriate symbol for an integer between 0 and 9999 inclusive using the Cistercian numbering system. The window size is hardcoded to 1280x720 pixels.
