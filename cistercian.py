import pygame

# This Button implementation is based on the following source:
# https://youtu.be/4_9twnEduFA


class Button:
    def __init__(self, color, x, y, width, height, text=''):
        self.color = color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text

    def draw(self, surface, outline=None):
        if outline:
            pygame.draw.rect(surface, outline, (self.x - 2, self.y - 2, self.width + 4, self.height + 4), 0)

        pygame.draw.rect(surface, self.color, (self.x, self.y, self.width, self.height), 0)

        if self.text != '':
            font = pygame.font.SysFont('arial', 50)
            text = font.render(self.text, True, (0, 0, 0))
            surface.blit(text, (self.x + (self.width / 2 - text.get_width() / 2),
                                self.y + (self.height / 2 - text.get_height() / 2)))

    def isOver(self, pos):
        if self.x < pos[0] < self.x + self.width:
            if self.y < pos[1] < self.y + self.height:
                return True

        return False


pygame.init()
pygame.display.set_caption('Cistercian Counter')

running = True
environment = pygame.Surface((960, 720))
panel = pygame.Surface((310, 720))
screen = pygame.display.set_mode((1280, 720))

ciphers = {
    0: [],
    1: [(0, 0), (160, 0)],
    2: [(0, 160), (160, 160)],
    3: [(0, 0), (160, 160)],
    4: [(0, 160), (160, 0)],
    5: [(0, 0), (160, 0), (0, 160)],
    6: [(160, 0), (160, 160)],
    7: [(0, 0), (160, 0), (160, 160), (160, 0)],
    8: [(0, 160), (160, 160), (160, 0), (160, 160)],
    9: [(0, 0), (160, 0), (160, 160), (0, 160)]
}

blitdict = {
    1: (480, 120),
    2: (320, 120),
    3: (480, 440),
    4: (320, 440)
}


def symbolize(n, d):
    p = 10 ** (d - 1)
    digit = int(n / p) % 10
    cipher = ciphers.get(digit)
    s = pygame.Surface((160, 160))
    s.fill((255, 255, 255))
    if digit > 0:
        pygame.draw.polygon(s, (0, 0, 0), cipher, 11)
    if d == 1:
        pygame.draw.line(s, (0, 0, 0), (0, 0), (0, 160), 11)
    if d == 2:
        s = pygame.transform.flip(s, True, False)
        pygame.draw.line(s, (0, 0, 0), (160, 0), (160, 160), 11)
    if d == 3:
        s = pygame.transform.flip(s, False, True)
        pygame.draw.line(s, (0, 0, 0), (0, 0), (0, 160), 11)
    if d == 4:
        s = pygame.transform.flip(s, True, True)
        pygame.draw.line(s, (0, 0, 0), (160, 0), (160, 160), 11)
    return s


buttons = [
    Button((255, 155, 155), 5, 10, 150, 100, '-1'),
    Button((155, 155, 255), 155, 10, 150, 100, '+1'),
    Button((255, 155, 155), 5, 160, 150, 100, '-10'),
    Button((155, 155, 255), 155, 160, 150, 100, '+10'),
    Button((255, 155, 155), 5, 310, 150, 100, '-100'),
    Button((155, 155, 255), 155, 310, 150, 100, '+100'),
    Button((255, 155, 155), 5, 460, 150, 100, '-1000'),
    Button((155, 155, 255), 155, 460, 150, 100, '+1000')
]

number = int(0)
panelpos = (970, 0)

while running:
    screen.fill((205, 205, 205))
    environment.fill((255, 255, 255))
    panel.fill((205, 255, 205))

    pygame.draw.line(environment, (0, 0, 0), (480, 120), (480, 599), 11)

    digits = len(str(number))

    if digits > 4:
        break

    while digits > 0:
        symbol = symbolize(int(number), digits)
        pygame.Surface.blit(environment, symbol, blitdict.get(digits))
        digits -= 1

    for b in buttons:
        b.draw(panel, (0, 0, 0))

    nfont = pygame.font.SysFont('arial', 60)
    ntext = nfont.render(str(number), True, (0, 0, 0))
    if len(str(number)) == 4:
        panel.blit(ntext, (86, 600))
    if len(str(number)) == 3:
        panel.blit(ntext, (104, 600))
    if len(str(number)) == 2:
        panel.blit(ntext, (122, 600))
    if len(str(number)) == 1:
        panel.blit(ntext, (136, 600))

    pygame.Surface.blit(screen, environment, (0, 0))
    pygame.Surface.blit(screen, panel, panelpos)
    pygame.display.flip()

    for event in pygame.event.get():
        mouse = pygame.mouse.get_pos()
        mousepanel = (mouse[0] - panelpos[0], mouse[1] - panelpos[1])

        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            for button in buttons:
                if button.isOver(mousepanel):
                    index = buttons.index(button)
                    quotient = int(index / 2)
                    remainder = index % 2
                    if remainder:
                        number += 10**quotient
                        if number > 9999:
                            number = 9999
                    else:
                        number -= 10**quotient
                        if number < 0:
                            number = 0
        if event.type == pygame.MOUSEMOTION:
            for button in buttons:
                index = buttons.index(button)
                remainder = index % 2
                if button.isOver(mousepanel):
                    if remainder:
                        button.color = (55, 55, 155)
                    else:
                        button.color = (155, 55, 55)
                else:
                    if remainder:
                        button.color = (155, 155, 255)
                    else:
                        button.color = (255, 155, 155)

pygame.quit()
